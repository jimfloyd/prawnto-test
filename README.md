# Module3 Project Gamma
PRAWNTO

### About The Project
Our FastAPI application is an education tool that facilitates CRUD operations.

### User Stories
Features:

A user can sign up, login and is directed to the explore page that displays all available exams.

A user can then create an exam by navigating to the "manage page" via dropdown menu, on the top right of the page their is a make exam button.

The "make exam page" prompts users to enter exam title and exam description. Pressing the create exam button on the page directs users to "make exam questions page."

Here, users can enter inputs for question texts and provide four answer options. They can select the correct answer via toggle button next to the selected options.

After entering the prompted data, there are two buttons to choose from, create question or submit exam. If you press the create question button, it will prompt you for another question and options. Once users have enough questions and they select submit exam they are directed back to "my created exams page."

On the "created exams page" all exams by user are displayed and they can select view gradebook for the particular exam. That directs the exam creator to view all exam attempts showing username, students score, and attempt number.

Users can use the dropdown feature to select explore, taking them back to welcome page that displays all available exams. They then can select the exam of their choice and press the take exam button that directs them to the "take exam" page where multiple choice question and answers are displayed. After answering questions they select submit exam and are directed to the "attempted exams page" that shows all exams the user has attempted.

### Intended Market
Our initial intended market includes anyone that is looking to create an exam to verify understanding of a topic, no matter the scale. We welcome the church pastor who needs to verify someone's understanding of lighting and sound for a church service, and we welcome a fortune 500 company looking to have their thousands of employees take a compliance certification. Our other initial target market is for anyone looking to gain knowledge and get confirmation of their understanding of said information. This could allow people to show proof of their capabilities to their current or future employers. Our future target market includes schools of all sizes: elementary to postgrad. The future of Prawnto is a full service education platform with not only exams, but educational materials, student to teacher communication, student to student communication, and the ability to show student improvement over time, allowing any educator to feel more confident in their teaching.

## Stretch Goals

We think that this project could be expanded quite easily into a more full fledged education platform. To that end, we believe that future improvements to the platform could include:
- The addition of classes to the platform, allowing there to be a class owner, and students could belong to the class.
- Exams could be made available to students based off the class to which they belong.
- We could add the capacity to store and show profile pictures.
- We could add the capacity to store and show pictures associated with exams and/or classes.
- We could add the ability to allow teachers to present learning materials to students in their classes, like links to required readings, flash cards, copies of class powerpoints, etc.
- We could add more robust views into exam attempts, showing the student the attempt number for that specific exam for that student, instead of the existing exam_attempt_id that is displayed.
- We could show the average grade of an exam to the teacher to provide insights into where they could spend additional time teaching.
- We could provide even more granular views into success rates based on each question, allowing the teacher to know where students may be struggling.
- Provide notifications to users when new exams are created, so they can take them *prawnto*
- Connect to ChapGPT API to have it grade essay questions for students.
- Add the capacity to have different point values for different exam questions.
- Add the ability to have questions with multiple checkboxes required for being a correct answer.
- Add the ability to increase or decrease the number of questions on the create question page.
- Create a page to show certificates of passed exams for proof for employers.
- Create student message boards


## Tech Stack
The tech stack for Prawnto includes:
- FastAPI for back end
- Python for backend
- PostgreSQL for the database tables
- VITE/React for front end management
- JavaScript for front end logic
- Tailwind for making the front end CSS pretty
- Docker for containerization
- Gitlab used for deployment and CI/CD
- Cirrus used for server deployment


## Install Extensions

-   Prettier: <https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode>
-   Black Formatter: <https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter>

## Deliverables

-   [ ] Wire-frame diagrams
-   [ ] API documentation
-   [ ] Project is deployed to Caprover (BE, DB) & GitLab-pages (FE)
-   [ ] GitLab issue board is setup and in use (or project management tool of choice)
-   [ ] Journals

## Project layout

The layout of the project is just like all of the projects
you did with `docker-compose` in module #2. You will create
a directory in the root of the repository for each service
that you add to your project just like those previous
projects were setup.

### Directories

Several directories have been added to your project. The
directories `docs` and `journals` are places for you and
your team-mates to, respectively, put any documentation
about your project that you create and to put your
project-journal entries. See the _README.md_ file in each
directory for more info.

The other directories, `ghi` and `api`, are services, that
you can start building off of.

Inside of `ghi` is a minimal React app that has an "under construction" page.
This app is written using the [Vite](https://vitejs.dev/) bundler. The example
code is also using [jsdoc](https://jsdoc.app/) to provide type hints for
JavaScript. You are not required to use JSDoc yourself, and you will be removing
these examples and providing your own code for `App.jsx`

Inside of `api` is a minimal FastAPI application.
"Where are all the files?" you might ask? Well, the
`main.py` file is the whole thing, and go take look inside
of it... There's not even much in there..., hmm? That is
FastAPI, we'll learn more about it in the coming days. Can
you figure out what this little web-application does even
though you haven't learned about FastAPI yet?

Also in `api` is a directory for your migrations.
If you choose to use PostgreSQL, then you'll want to use
migrations to control your database. Unlike Django, where
migrations were automatically created for you, you'll write
yours by hand using DDL. Don't worry about not knowing what
DDL means; we have you covered. There's a sample migration
in there that creates two tables so you can see what they
look like.

The Dockerfile and Dockerfile.dev run your migrations
for you automatically.

### Installing python dependencies locally

In order for VSCode's built in code completion and intelligence to
work correctly, it needs the dependencies from the requirements.txt file
installed. We do this inside docker, but not in the workspace.

So we need to create a virtual environment and pip install the requirements.

From inside the `api` folder:

```bash
python -m venv .venv
```

Then activate the virtual environment

```bash
source .venv/bin/activate
```

And finally install the dependencies

```bash
pip install -r requirements.txt
```

Then make sure the venv is selected in VSCode by checking the lower right of the
VSCode status bar

### Other files

The following project files have been created as a minimal
starting point. Please follow the guidance for each one for
a most successful project.

-   `docker-compose.yaml`: there isn't much in here, just a
    **really** simple UI and FastAPI service. Add services
    (like a database) to this file as you did with previous
    projects in module #2.
-   `.gitlab-ci.yml`: This is your "ci/cd" file where you will
    configure automated unit tests, code quality checks, and
    the building and deployment of your production system.
    Currently, all it does is deploy an "under construction"
    page to your production UI on GitLab and a sample backend
    to CapRover. We will learn much more about this file.
-   `.gitignore`: This is a file that prevents unwanted files
    from getting added to your repository, files like
    `pyc` files, `__pycache__`, etc. We've set it up so that
    it has a good default configuration for Python projects.
-   `.env.sample`: This file is a template to copy when
    creating environment variables for your team. Create a
    copy called `.env` and put your own passwords in here
    without fear of it being committed to git (see `.env`
    listed in `.gitignore`). You can also put team related
    environment variables in here, things like api and signing
    keys that shouldn't be committed; these should be
    duplicated in your deployed environments.

## How to complete the initial deploy

There will be further guidance on completing the initial
deployment, but it just consists of these steps:

### Setup GitLab repo/project

-   make sure this project is in a group. If it isn't, stop
    now and move it to a GitLab group
-   remove the fork relationship: In GitLab go to:

    Settings -> General -> Advanced -> Remove fork relationship

-   add these GitLab CI/CD variables:
    -   PUBLIC_URL : this is your gitlab pages URL
    -   VITE_APP_API_HOST: enter "blank" for now

#### Your GitLab pages URL

You can't find this in GitLab until after you've done a deploy
but you can figure it out yourself from your GitLab project URL.

If this is your project URL

https://gitlab.com/GROUP_NAME/PROJECT_NAME

then your GitLab pages URL will be

https://GROUP_NAME.gitlab.io/PROJECT_NAME

### Initialize CapRover

1. Attain IP address and domain from an instructor
1. Follow the steps in the CD Cookbook in Learn.

### Update GitLab CI/CD variables

Copy the service URL for your CapRover service and then paste
that into the value for the REACT_APP_API_HOST CI/CD variable
in GitLab.

### Deploy it

Merge a change into main to kick off the initial deploy. Once the build pipeline
finishes you should be able to see an "under construction" page on your GitLab
pages site.
