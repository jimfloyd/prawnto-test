steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE Exam_Attempts (
            attempt_id SERIAL PRIMARY KEY,
            user_id INT REFERENCES Accounts_with_hashed_passwords(user_id),
            exam_id INT REFERENCES Exams(exam_id),
            score INT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE Exam_Attempts;
        """,
    ]
]
