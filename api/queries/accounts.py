from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str
    first_name: str
    last_name: str
    email: str


class AccountOut(BaseModel):
    user_id: int
    username: str
    first_name: str
    last_name: str
    email: str


class AccountOutWithPassword(AccountOut):
    username: str
    first_name: str
    last_name: str
    email: str
    hashed_password: str


class AccountQueries:
    def get(self, username: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM Accounts_with_hashed_passwords
                    WHERE username = %s;
                    """,
                    [username],
                )
                record = result.fetchone()

                if record is None:
                    return None
                return AccountOutWithPassword(
                    user_id=record[0],
                    hashed_password=record[1],
                    first_name=record[2],
                    last_name=record[3],
                    username=record[4],
                    email=record[5],
                )

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        if AccountQueries.get(self, info.username) is None:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO Accounts_with_hashed_passwords (
                            hashed_password,
                            first_name,
                            last_name,
                            username,
                            email
                        )
                        VALUES (
                        %s,
                        %s,
                        %s,
                        %s,
                        %s
                        )
                        RETURNING user_id;
                        """,
                        [
                            hashed_password,
                            info.first_name,
                            info.last_name,
                            info.username,
                            info.email,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = info.dict()
                    return AccountOutWithPassword(
                        user_id=id, hashed_password=hashed_password, **old_data
                    )
        else:
            raise DuplicateAccountError()

    def getUsername(self, user_id: int) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM Accounts_with_hashed_passwords
                    WHERE user_id = %s;
                    """,
                    [user_id],
                )
                record = result.fetchone()

                if record is None:
                    return None
                new_usernameOut = AccountOutWithPassword(
                    user_id=record[0],
                    hashed_password=record[1],
                    first_name=record[2],
                    last_name=record[3],
                    username=record[4],
                    email=record[5],
                )
                return {"user": new_usernameOut}
