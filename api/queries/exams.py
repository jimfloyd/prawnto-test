from pydantic import BaseModel
from queries.pool import pool


class ExamIn(BaseModel):
    exam_name: str
    description: str
    creator_id: int


class ExamOut(BaseModel):
    exam_id: int
    exam_name: str
    description: str
    creator_id: int


class ExamOutList(BaseModel):
    exams: list[ExamOut]


class ExamQueries:
    def create(self, info: ExamIn) -> ExamOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO Exams (
                            exam_name,
                            description,
                            creator_id
                        )
                        VALUES (
                        %s,
                        %s,
                        %s
                        )
                        RETURNING exam_id;
                    """,
                    [
                        info.exam_name,
                        info.description,
                        info.creator_id,
                    ],
                )
                id = result.fetchone()[0]
                old_data = info.dict()
                return ExamOut(exam_id=id, **old_data)

    def get_exams(self) -> ExamOutList:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT (
                            exam_id,
                            exam_name,
                            description,
                            creator_id
                        )
                        FROM Exams;
                    """,
                )
                records = result.fetchall()
                ExamOut_list = []
                for record in records:
                    new_ExamOut = ExamOut(
                        exam_id=record[0][0],
                        exam_name=record[0][1],
                        description=record[0][2],
                        creator_id=record[0][3],
                    )
                    ExamOut_list.append(new_ExamOut)
                return ExamOutList(exams=ExamOut_list)

    def get_exams_by_creator(self, creator_id: int) -> ExamOutList:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT (
                            exam_id,
                            exam_name,
                            description,
                            creator_id
                        )
                        FROM Exams WHERE creator_id = %s;
                    """,
                    [creator_id],
                )
                records = result.fetchall()
                ExamOut_list = []
                for record in records:
                    new_ExamOut = ExamOut(
                        exam_id=record[0][0],
                        exam_name=record[0][1],
                        description=record[0][2],
                        creator_id=creator_id,
                    )
                    ExamOut_list.append(new_ExamOut)
                return ExamOutList(exams=ExamOut_list)

    def get_exam_details_by_exam_id(self, exam_id: int) -> ExamOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT (
                            exam_id,
                            exam_name,
                            description,
                            creator_id
                        )
                        FROM Exams WHERE exam_id = %s;
                    """,
                    [exam_id],
                )
                record = result.fetchone()
                new_ExamOut = ExamOut(
                    exam_id=record[0][0],
                    exam_name=record[0][1],
                    description=record[0][2],
                    creator_id=record[0][3],
                )
                return new_ExamOut
