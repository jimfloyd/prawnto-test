from fastapi import Depends, APIRouter
from authenticator import authenticator
from queries.exams import ExamIn, ExamOut, ExamQueries

router = APIRouter()


@router.post("/api/exams")
async def create_exam(
    info: ExamIn,
    exam_queries: ExamQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> ExamOut:
    exam = exam_queries.create(info)
    return exam


@router.get("/api/exams")
async def get_all_exams(
    exam_queries: ExamQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    exam = exam_queries.get_exams()
    return exam


@router.get("/api/exams/created/{creator_id}")
async def get_exams_by_creator(
    creator_id: int,
    exam_queries: ExamQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    exam = exam_queries.get_exams_by_creator(creator_id)
    return exam


@router.get("/api/exams/{exam_id}")
async def get_exam_details_by_exam_id(
    exam_id: int,
    exam_queries: ExamQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    exam = exam_queries.get_exam_details_by_exam_id(exam_id)
    return exam
