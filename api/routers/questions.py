from fastapi import Depends, APIRouter
from authenticator import authenticator
from queries.questions import QuestionIn, QuestionOut, QuestionQueries

router = APIRouter()


@router.post("/api/exams/{exam_id}")
async def create_question(
    info: QuestionIn,
    exam_id: int,
    question_queries: QuestionQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> QuestionOut:
    question = question_queries.create(info, exam_id)
    return question


@router.get("/api/questions/{exam_id}")
async def get_questions_by_exam(
    exam_id: int,
    question_queries: QuestionQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    question = question_queries.get_question_by_exam(exam_id)
    return question


@router.get("/api/questions/count/{exam_id}")
async def get_question_count_by_exam(
    exam_id: int,
    question_queries: QuestionQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    question_count = question_queries.get_question_count_by_exam(exam_id)
    return question_count
