# *******************************************
# *******************************************
# *******************************************
# Unit Test made by Jeremy Kilcrease
# *******************************************
# *******************************************
# *******************************************

from authenticator import authenticator
from main import app
from queries.exams import ExamOut
from fastapi.testclient import TestClient
from queries.exam_attempts import ExamAttemptQueries
from queries.accounts import AccountOut


client = TestClient(app)


class MockExamAttemptQueries:
    def get_exams_by_attempt_id(self, attempt_id: int) -> ExamOut:
        return ExamOut(
            exam_id=1,
            exam_name="Math",
            description="Addition",
            creator_id=1,
        )


def fake_get_current_account_data():
    return AccountOut(
        user_id=1,
        username="Bob",
        first_name="Billy",
        last_name="Bob",
        email="bob@billy.com",
    )


def test_get_one_exam_by_attempt_id():
    app.dependency_overrides[ExamAttemptQueries] = MockExamAttemptQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    attempt_id = 1
    response = client.get(f"/api/exam_attempts/exam/{attempt_id}")
    expected_response = {
        "exam_id": 1,
        "exam_name": "Math",
        "description": "Addition",
        "creator_id": 1,
    }
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected_response
