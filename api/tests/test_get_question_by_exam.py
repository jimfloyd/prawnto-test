# *******************************************
# *******************************************
# *******************************************
# Unit Test made by Denver Alexander
# *******************************************
# *******************************************
# *******************************************

from main import app
from authenticator import authenticator
from queries.accounts import AccountOut
from fastapi.testclient import TestClient
from queries.questions import QuestionOut, QuestionOutList, QuestionQueries


client = TestClient(app)


class MockQuestionQueries:
    def get_question_by_exam(self, exam_id: int) -> QuestionOutList:
        question_one = QuestionOut(
            question_id=1,
            exam_id=1,
            question_text="Who is the coolest of them all?",
        )
        question_two = QuestionOut(
            question_id=2,
            exam_id=1,
            question_text="Could Denver really be the coolest?",
        )
        question_list = [question_one, question_two]
        return QuestionOutList(questions=question_list)


def fake_get_current_account_data():
    return AccountOut(
        user_id=1,
        username="denveriv",
        first_name="Denver",
        last_name="Alexander",
        email="denveriv@hotmail.com",
    )


def test_get_questions_by_exam_id():
    app.dependency_overrides[QuestionQueries] = MockQuestionQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    exam_id = 1

    response = client.get(f"/api/questions/{exam_id}")

    app.dependency_overrides = {}

    expected_response = {
        "questions": [
            {
                "question_id": 1,
                "exam_id": 1,
                "question_text": "Who is the coolest of them all?",
            },
            {
                "question_id": 2,
                "exam_id": 1,
                "question_text": "Could Denver really be the coolest?",
            },
        ]
    }
    assert response.status_code == 200
    assert response.json() == expected_response
