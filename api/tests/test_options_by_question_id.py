# *******************************************
# *******************************************
# *******************************************
# Unit Test made by Sam Madvig
# *******************************************
# *******************************************
# *******************************************

from authenticator import authenticator
from main import app
from queries.accounts import AccountOut
from fastapi.testclient import TestClient
from queries.options import OptionQueries, OptionOutList, OptionOut


client = TestClient(app)


class MockOptionsQueries:
    def get_options_by_question_id(self, question_id: int) -> OptionOutList:
        option_one = OptionOut(
            question_id=10,
            option_text="This would be a wrong answer.",
            correct_bool=False,
            option_id=37,
        )
        option_two = OptionOut(
            question_id=10,
            option_text="This would also be a wrong answer.",
            correct_bool=False,
            option_id=38,
        )
        option_three = OptionOut(
            question_id=10,
            option_text="This is the right answer.",
            correct_bool=True,
            option_id=39,
        )
        option_four = OptionOut(
            question_id=10,
            option_text="This would be the third wrong answer.",
            correct_bool=False,
            option_id=40,
        )
        option_list = [option_one, option_two, option_three, option_four]
        return OptionOutList(options=option_list)


def fake_get_current_account_data():
    return AccountOut(
        user_id=12,
        username="Sammad",
        first_name="Sam",
        last_name="Madvig",
        email="sam.mad@comcast.net",
    )


def test_get_options_by_question():
    # Arrange
    app.dependency_overrides[OptionQueries] = MockOptionsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    question_id = 10

    # Act
    response = client.get(f"/api/options/{question_id}")

    # Cleanup
    app.dependency_overrides = {}

    # Assert
    expected_response = {
        "options": [
            {
                "question_id": 10,
                "option_text": "This would be a wrong answer.",
                "correct_bool": False,
                "option_id": 37,
            },
            {
                "question_id": 10,
                "option_text": "This would also be a wrong answer.",
                "correct_bool": False,
                "option_id": 38,
            },
            {
                "question_id": 10,
                "option_text": "This is the right answer.",
                "correct_bool": True,
                "option_id": 39,
            },
            {
                "question_id": 10,
                "option_text": "This would be the third wrong answer.",
                "correct_bool": False,
                "option_id": 40,
            },
        ]
    }

    assert response.status_code == 200
    assert response.json() == expected_response
