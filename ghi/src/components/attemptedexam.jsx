import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ocean_background from '/src/images/ocean_background.png'

const API_HOST = import.meta.env.VITE_API_HOST

function AttemptedExam() {
    const [examAttempts, setExamAttempts] = useState([])
    const [examNames, setExamNames] = useState({})
    const [token, setToken] = useState("unassigned")
    const navigate = useNavigate()

    const getData = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        const tokenJson = await tokenResponse.json()
        const user_id = tokenJson.account.user_id

        const response = await fetch(
            `${API_HOST}/api/exam_attempts/user/${user_id}`,
            { credentials: 'include' }
        )
        if (response.ok) {
            const data = await response.json()
            setExamAttempts(data.exam_attempts)
        } else {
            console.error('Request Error')
        }
    }
    const getExamDetails = async (exam_id) => {
        const response = await fetch(`${API_HOST}/api/exams/${exam_id}`, {
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            return data.exam_name
        } else {
            console.error('Request Error')
        }
    }

    const fetchExamNames = async () => {
        const examNamesMap = {}
        for (const attempt of examAttempts) {
            const examName = await getExamDetails(attempt.exam_id)
            examNamesMap[attempt.exam_id] = examName
        }
        setExamNames(examNamesMap)
    }

    const findToken = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        if (tokenResponse.ok) {
            const tokenJson = await tokenResponse.json()
            if (tokenJson === null) {
                setToken(null)
            } else {
                setToken(tokenJson.access_token)
            }
        } else {
            console.error('There was an error fetching the token.')
        }
    }


   const checkStatus = async (token) => {
       if (!token) {
           navigate('/login')
       }
   }

    useEffect(() => {
        findToken()
        getData()
    }, [])
    useEffect(() => {
        checkStatus(token)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])
    useEffect(() => {
        if (examAttempts.length > 0) {
            fetchExamNames()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [examAttempts])

    return (
        <>
            <div
                className="bg-fixed bg-cover bg-center min-h-screen pt-40 pb-40"
                style={{ backgroundImage: `url(${ocean_background})` }}
            >
                <h1 className="text-6xl text-center justify-center text-white font-bold pb-12">
                    Exam Attempts
                </h1>
                <div>
                    <table className="mx-auto table-auto">
                        <thead>
                            <tr className="bg-slate-900 text-red-500">
                                <th className="border border-slate-500 p-2">
                                    Exam Title
                                </th>
                                <th className="border border-slate-500 p-2">
                                    Student Score
                                </th>
                                <th className="border border-slate-500 p-2">
                                    Attempt #
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {examAttempts.filter((examAttempt) => examAttempt.score != null).map((examAttempt) => {
                                // getExamDetails(examAttempt.exam_id)
                                return (
                                    <tr
                                        className="text-center items-center justify-center even:bg-white odd:dark:bg-gray-200 border-b dark:border-gray-700"
                                        key={examAttempt.attempt_id}
                                    >
                                        <td className="border border-black p-2">
                                            {examNames[examAttempt.exam_id]}
                                        </td>
                                        <td className="border border-black p-2">
                                            {examAttempt.score}
                                        </td>
                                        <td className="border border-black p-2">
                                            {examAttempt.attempt_id}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}

export default AttemptedExam
