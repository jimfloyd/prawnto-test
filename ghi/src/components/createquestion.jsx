import { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import ocean_background from '../images/ocean_background.png'
const API_HOST = import.meta.env.VITE_API_HOST

function CountTrue(a, b, c, d) {
    let count = 0
    if (a) {
        count++
    }
    if (b) {
        count++
    }
    if (c) {
        count++
    }
    if (d) {
        count++
    }
    return count
}

function CreateQuestion() {
    const initData = {
        question_text: '',
    }

    const [textFormData, setTextFormData] = useState(initData)
    const [optionOneTextData, setOptionOneTextData] = useState('')
    const [optionTwoTextData, setOptionTwoTextData] = useState('')
    const [optionThreeTextData, setOptionThreeTextData] = useState('')
    const [optionFourTextData, setOptionFourTextData] = useState('')
    const [checkedOne, setCheckedOne] = useState(false)
    const [checkedTwo, setCheckedTwo] = useState(false)
    const [checkedThree, setCheckedThree] = useState(false)
    const [checkedFour, setCheckedFour] = useState(false)
    const [token, setToken] = useState("unassigned")

    const examId = useParams().exam_id
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        if (
            CountTrue(checkedOne, checkedTwo, checkedThree, checkedFour) === 0
        ) {
            console.error('please select one right answer')
        } else if (
            CountTrue(checkedOne, checkedTwo, checkedThree, checkedFour) > 1
        ) {
            console.error('please select only one right answer')
        } else {
            const questionFetchConfig = {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify(textFormData),
                headers: {
                    'Content-Type': 'application/json',
                },
            }

            const questionUrl = `${API_HOST}/api/exams/${examId}`
            const questionResponse = await fetch(
                questionUrl,
                questionFetchConfig
            )

            const questionId = await questionResponse.json()

            const optionUrl = `${API_HOST}/api/options/${questionId.question_id}`

            const optionOne = {
                option_text: optionOneTextData,
                correct_bool: checkedOne,
            }
            const optionTwo = {
                option_text: optionTwoTextData,
                correct_bool: checkedTwo,
            }
            const optionThree = {
                option_text: optionThreeTextData,
                correct_bool: checkedThree,
            }
            const optionFour = {
                option_text: optionFourTextData,
                correct_bool: checkedFour,
            }

            const optionOneFetchConfig = {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify(optionOne),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const optionTwoFetchConfig = {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify(optionTwo),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const optionThreeFetchConfig = {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify(optionThree),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const optionFourFetchConfig = {
                method: 'post',
                credentials: 'include',
                body: JSON.stringify(optionFour),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const optionOneResponse = await fetch(
                optionUrl,
                optionOneFetchConfig
            )
            const optionTwoResponse = await fetch(
                optionUrl,
                optionTwoFetchConfig
            )
            const optionThreeResponse = await fetch(
                optionUrl,
                optionThreeFetchConfig
            )
            const optionFourResponse = await fetch(
                optionUrl,
                optionFourFetchConfig
            )

            if (
                optionOneResponse.ok &&
                optionTwoResponse.ok &&
                optionThreeResponse.ok &&
                optionFourResponse.ok
            ) {
                setOptionOneTextData('')
                setOptionTwoTextData('')
                setOptionThreeTextData('')
                setOptionFourTextData('')
                setTextFormData(initData)
                setCheckedOne(false)
                setCheckedTwo(false)
                setCheckedThree(false)
                setCheckedFour(false)
            }
        }
    }

    const finishExam = async (event) => {
        handleSubmit(event)
        navigate('/manage')
    }

    const handleTextFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setTextFormData({ ...textFormData, [inputName]: value })
    }
    const handleOptionOneChange = (event) => {
        const value = event.target.value
        setOptionOneTextData(value)
    }
    const handleOptionTwoChange = (event) => {
        const value = event.target.value
        setOptionTwoTextData(value)
    }
    const handleOptionThreeChange = (event) => {
        const value = event.target.value
        setOptionThreeTextData(value)
    }
    const handleOptionFourChange = (event) => {
        const value = event.target.value
        setOptionFourTextData(value)
    }

   const checkStatus = async (token) => {
       if (!token) {
           navigate('/login')
       }
   }

   const findToken = async () => {
       const tokenResponse = await fetch(`${API_HOST}/token`, {
           credentials: 'include',
       })
       if (tokenResponse.ok) {
           const tokenJson = await tokenResponse.json()
           if (tokenJson === null) {
               setToken(null)
           } else {
               setToken(tokenJson.access_token)
           }
       } else {
           console.error('There was an error fetching the token.')
       }
   }

   useEffect(() => {
    findToken()
   }, [])
   useEffect(() => {
    checkStatus(token)
    // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [token])

    return (
        <div
            className="bg-fixed bg-cover bg-center min-h-screen pt-40 pb-40"
            style={{ backgroundImage: `url(${ocean_background})` }}
        >
            <div className="p-8 bg-white max-w-lg mx-auto my-10 border border-slate-400 rounded-lg shadow-lg">
                <h1 className="text-3xl">Create Question</h1>
                <form onSubmit={handleSubmit} id="create-exam-form">
                    <label htmlFor="question_text: ">Question</label>
                    <div>
                        <input
                            name="question_text"
                            type="text"
                            required
                            value={textFormData.question_text}
                            className="border-2 border-gray-300 p-2 w-full rounded"
                            onChange={handleTextFormChange}
                        />
                    </div>
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <th>Options</th>
                                    <th>Select Correct Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <label htmlFor="option_text: ">
                                            Option 1:
                                        </label>
                                        <input
                                            name="option_text"
                                            type="text"
                                            required
                                            value={optionOneTextData}
                                            className="border-2 border-gray-300 p-2 w-full rounded"
                                            onChange={handleOptionOneChange}
                                        />
                                    </td>
                                    <td className="pl-12 pt-8">
                                        <label className="relative inline-flex items-center cursor-pointer">
                                            <input
                                                type="checkbox"
                                                checked={checkedOne}
                                                value={checkedOne}
                                                onChange={() => {
                                                    setCheckedOne(!checkedOne)
                                                }}
                                                name="correct_bool"
                                                className="sr-only peer"
                                            />
                                            <div className=" w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                                            <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="option_text: ">
                                            Option 2:
                                        </label>
                                        <input
                                            name="option_text"
                                            type="text"
                                            required
                                            value={optionTwoTextData}
                                            className="border-2 border-gray-300 p-2 w-full rounded"
                                            onChange={handleOptionTwoChange}
                                        />
                                    </td>
                                    <td className="pl-12 pt-8">
                                        <label className="relative inline-flex items-center cursor-pointer">
                                            <input
                                                type="checkbox"
                                                checked={checkedTwo}
                                                value={checkedTwo}
                                                onChange={() => {
                                                    setCheckedTwo(!checkedTwo)
                                                }}
                                                className="sr-only peer"
                                            />
                                            <div className="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                                            <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="option_text: ">
                                            Option 3:
                                        </label>
                                        <input
                                            name="option_text"
                                            type="text"
                                            required
                                            value={optionThreeTextData}
                                            className="border-2 border-gray-300 p-2 w-full rounded"
                                            onChange={handleOptionThreeChange}
                                        />
                                    </td>
                                    <td className="pl-12 pt-8">
                                        <label className="relative inline-flex items-center cursor-pointer">
                                            <input
                                                type="checkbox"
                                                checked={checkedThree}
                                                value={checkedThree}
                                                onChange={() => {
                                                    setCheckedThree(
                                                        !checkedThree
                                                    )
                                                }}
                                                className="sr-only peer"
                                            />
                                            <div className="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                                            <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="option_text: ">
                                            Option 4:
                                        </label>
                                        <input
                                            name="option_text"
                                            type="text"
                                            required
                                            value={optionFourTextData}
                                            className="border-2 border-gray-300 p-2 w-full rounded"
                                            onChange={handleOptionFourChange}
                                        />
                                    </td>
                                    <td className="pl-12 pt-8">
                                        <label className="relative inline-flex items-center cursor-pointer">
                                            <input
                                                type="checkbox"
                                                checked={checkedFour}
                                                value={checkedFour}
                                                onChange={() => {
                                                    setCheckedFour(!checkedFour)
                                                }}
                                                className="sr-only peer"
                                            />
                                            <div className="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                                            <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300"></span>
                                        </label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="justify-center space-x-2 items-center text-center mt-8">
                        <button
                            type="submit"
                            className="bg-slate-900 text-red-400 font-bold px-4 py-2 rounded shadow-sm shadow-slate-900 border border-slate-500 hover:bg-slate-500"
                        >
                            Create question
                        </button>
                        <button
                            type="submit"
                            onClick={finishExam}
                            className="bg-slate-900 text-red-400 font-bold px-4 py-2 rounded shadow-sm shadow-slate-900 border border-slate-500 hover:bg-slate-500"
                        >
                            Submit Exam
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default CreateQuestion
