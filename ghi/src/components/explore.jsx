import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
const API_HOST = import.meta.env.VITE_API_HOST
import ocean_background from '../images/ocean_background.png'

const Explore = () => {
    const [exams, setExams] = useState([])
    const [usernames, setUsernames] = useState({})
    const [counts, setCounts] = useState({})
    const navigate = useNavigate()
    const [token, setToken] = useState('unassigned')

    const findToken = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        if (tokenResponse.ok) {
            const tokenJson = await tokenResponse.json()
            if (tokenJson === null) {
                setToken(null)
            } else {
                setToken(tokenJson.access_token)
            }
        } else {
            console.error('There was an error fetching the token.')
        }
    }

    const getData = async () => {
        const response = await fetch(`${API_HOST}/api/exams/`, {
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            setExams(data.exams)
        } else {
            console.error('Request Error')
        }
    }

    const getCount = async (exam_id) => {
        const count = await fetch(
            `${API_HOST}/api/questions/count/${exam_id}`,
            {
                credentials: 'include',
            }
        )
        if (count.ok) {
            const data = await count.json()
            return data
        } else {
            console.error('Error getting count')
        }
    }

    const getUsername = async (user_id) => {
        try {
            const response = await fetch(
                `${API_HOST}/api/get_username/${user_id}`,
                { credentials: 'include' }
            )

            if (response.ok) {
                const data = await response.json()
                return data.user.username
            } else {
                console.error('Failed to get username')
            }
        } catch (error) {
            console.error('Error fetching username:', error)
        }
    }

    const fetchUsernames = async () => {
        const usernamesMap = {}
        const countsMap = {}
        for (const exam of exams) {
            const username = await getUsername(exam.creator_id)
            const count = await getCount(exam.exam_id)
            usernamesMap[exam.creator_id] = username
            countsMap[exam.exam_id] = count
        }
        setUsernames(usernamesMap)
        setCounts(countsMap)
    }

    const handleClick = async (e) => {
        e.preventDefault()
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        const tokenJson = await tokenResponse.json()
        const user_id = tokenJson.account.user_id
        const exam_id = e.target.value
        const fetchConfig = {
            method: 'POST',
            body: `{"user_id": ${user_id}}`,
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        }
        const url = `${API_HOST}/api/exam_attempts/${exam_id}`
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            const attemptId = data.attempt_id
            navigate(`/takeexam/${attemptId}`)
        } else {
            console.error('Error')
        }
    }
    const checkStatus = async (token) => {
        if (!token) {
            navigate('/login')
        }
    }

    useEffect(() => {
        findToken()
        getData()
    }, [])
    useEffect(() => {
        checkStatus(token)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])
    useEffect(() => {
        if (exams.length > 0) {
            fetchUsernames()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [exams])

        return (
            <div
                className="pt-32 bg-fixed bg-cover bg-center min-h-screen"
                style={{ backgroundImage: `url(${ocean_background})` }}
            >
                <div className="">
                    <div className="mb-12 text-center justify-center">
                        <h1 className="text-6xl text-white font-bold">
                            Explore all Exams
                        </h1>
                    </div>
                    <div></div>
                    <div className="pb-32">
                        <table className="mx-auto table-auto">
                            <thead>
                                <tr className="bg-slate-900 text-red-500">
                                    <th className="border border-slate-500 p-2">
                                        Exam
                                    </th>
                                    <th className="border border-slate-500 p-2">
                                        Description
                                    </th>
                                    <th className="border border-slate-500 p-2">
                                        Creator
                                    </th>
                                    <th className="border border-slate-500 p-2">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {exams.filter((exam) => counts[exam.exam_id] > 0)
                                    .map((exam) => (
                                        <tr
                                            className="text-center items-center justify-center even:bg-white odd:dark:bg-gray-200 border-b dark:border-gray-700"
                                            key={exam.exam_id}
                                        >
                                            <td className="border border-black p-2">
                                                {exam.exam_name}
                                            </td>
                                            <td className="border border-black p-2">
                                                {exam.description}
                                            </td>
                                            <td className="border border-black p-2">
                                                {usernames[exam.creator_id]}
                                            </td>
                                            <td className="border border-black p-2">
                                                <button
                                                    onClick={handleClick}
                                                    value={exam.exam_id}
                                                    className="bg-slate-900 text-red-500 font-medium text-center px-2 py-1 border border-slate-500"
                                                >
                                                    Take Exam
                                                </button>
                                            </td>
                                        </tr>
                                    ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }

export default Explore
