import prawnto_bg from '../images/prawnto_bg.png'
import { Link } from 'react-router-dom'
import useToken from '@galvanize-inc/jwtdown-for-react'

function Heropage() {
    const { token } = useToken()

    return (
        <div
            className="bg-fixed bg-cover bg-center h-auto"
            style={{ backgroundImage: `url(${prawnto_bg})` }}
        >
            <div className="relative h-screen">
                <div className="lg:max-w-xl md:max-w-md sm:max-w-sm pl-6">
                    <div className="text-5xl text-white pt-32 pl-20 font-bold">
                        <h1>Build and share exams Prawnto!</h1>
                    </div>
                    <div className="bg-white bg-opacity-60 rounded-xl p-12 mt-3 shadow-lg shadow-slate-700 border border-slate-900 text-slate-900 text-center">
                        <p className="opacity-100 font-bold text-lg">
                            Explore Prawnto – your user-friendly exam hub!
                        </p>
                        <h3>
                            <em>Sign up</em> for <b>free</b>, log in, and dive
                            into a world of diverse exams. Create and share
                            custom tests or find the perfect one to showcase
                            your expertise.
                        </h3>
                    </div>
                    <div className="flex justify-center mt-8">
                        <Link to={!token ? '/Signup' : '/explore'}>
                            <button className="p-4 bg-slate-900 rounded-xl text-red-500 text-xl font-bold hover:bg-slate-700 border border-slate-500">
                                <h1>Get Started!</h1>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Heropage
