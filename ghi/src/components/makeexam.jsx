import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ocean_background from '../images/ocean_background.png'
const API_HOST = import.meta.env.VITE_API_HOST

function MakeExam() {
    const navigate = useNavigate()
    const [token, setToken] = useState("unassigned")
    const [formData, setFormData] = useState({
        exam_name: '',
        description: '',
        creator_id: '',
    })

    const getUserId = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        const tokenJson = await tokenResponse.json()
        const userId = tokenJson.account.user_id
        setFormData({ ...formData, creator_id: userId })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const fetchConfig = {
            method: 'post',
            credentials: 'include',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const url = `${API_HOST}/api/exams`

        const response = await fetch(url, fetchConfig)
        const responseJson = await response.json()
        const examId = responseJson.exam_id
        navigate(`/exams/${examId}/createquestion`)
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({ ...formData, [inputName]: value })
    }

    const findToken = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        if (tokenResponse.ok) {
            const tokenJson = await tokenResponse.json()
            if (tokenJson === null) {
                setToken(null)
            } else {
                setToken(tokenJson.access_token)
            }
        } else {
            console.error('There was an error fetching the token.')
        }
    }


    const checkStatus = async (token) => {
       if (!token) {
        navigate('/login')
           }
       }

    useEffect(() => {
        findToken()
        getUserId()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() => {
        checkStatus(token)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])
    return (
        <div
            className="bg-fixed bg-cover bg-center min-h-screen pt-40 pb-40"
            style={{ backgroundImage: `url(${ocean_background})` }}
        >
            <div className="p-8 bg-white max-w-sm mx-auto my-10 border border-slate-400 rounded-lg shadow-lg">
                <h1 className="text-3xl">Make an Exam</h1>
                <form onSubmit={handleSubmit} id="create-exam-form">
                    <label htmlFor="first_name: ">Exam Title</label>
                    <div>
                        <input
                            name="exam_name"
                            type="text"
                            required
                            value={formData.exam_name}
                            className="border-2 border-gray-300 p-2 w-full rounded"
                            onChange={handleFormChange}
                        />
                    </div>
                    <label htmlFor="first_name: ">Exam Description</label>
                    <div>
                        <input
                            name="description"
                            type="text"
                            required
                            value={formData.description}
                            className="border-2 border-gray-300 p-2 w-full rounded"
                            onChange={handleFormChange}
                        />
                    </div>
                    <div className="justify-center items-center text-center mt-8">
                        <button
                            type="submit"
                            className="bg-slate-900 text-red-400 font-bold px-4 py-2 rounded shadow-sm shadow-slate-900 border border-slate-500 hover:bg-slate-500"
                        >
                            Create Exam
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default MakeExam
