import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import ocean_background from '../images/ocean_background.png'
const API_HOST = import.meta.env.VITE_API_HOST

function Manage() {
    const [exams, setExams] = useState([])
    const navigate = useNavigate()
    const [token, setToken] = useState('unassigned')
    const [counts, setCounts] = useState({})


    const getData = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        const tokenJson = await tokenResponse.json()
        const user_id = tokenJson.account.user_id

        const response = await fetch(
            `${API_HOST}/api/exams/created/${user_id}`,
            { credentials: 'include' }
        )
        if (response.ok) {
            const data = await response.json()
            setExams(data.exams)
        } else {
            console.error('Request Error')
        }
    }

    const getCount = async (exam_id) => {
        const count = await fetch(
            `${API_HOST}/api/questions/count/${exam_id}`,
            {
                credentials: 'include',
            }
        )
        if (count.ok) {
            const data = await count.json()
            return data
        } else {
            console.error('Error getting count')
        }
    }

    const fetchCounts = async () => {
        const countsMap = {}
        for (const exam of exams) {
            const count = await getCount(exam.exam_id)
            countsMap[exam.exam_id] = count
        }
        setCounts(countsMap)
    }

    const checkStatus = async (token) => {
        if(!token){
            navigate("/login")
        }

    }

    const findToken = async () => {
        const tokenResponse = await fetch(`${API_HOST}/token`, {
            credentials: 'include',
        })
        if (tokenResponse.ok) {
            const tokenJson = await tokenResponse.json()
            if (tokenJson === null) {
                setToken(null)
            } else {
                setToken(tokenJson.access_token)
            }
        } else {
            console.error('There was an error fetching the token.')
        }
    }

    useEffect(() => {
        findToken()
        getData()
    }, [])
    useEffect(() => {
        fetchCounts()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [exams])
    useEffect(() => {
        checkStatus(token)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])

    return (
        <div
            className="bg-fixed bg-cover bg-center min-h-screen pt-40 pb-40"
            style={{ backgroundImage: `url(${ocean_background})` }}
        >
            <h1 className="text-6xl text-center font-bold text-white">
                My Created Exams
            </h1>
            <div className="flex lg:justify-between md:justify-center sm: justify-center pt-6 pr-12">
                <div></div>
                <div className="px-4 py-2 bg-slate-900 rounded-xl text-red-400 font-medium hover:bg-slate-700">
                    <Link to="/makeexam" className="active">
                        Make New Exam
                    </Link>
                </div>
            </div>
            <div className="flex flex-wrap p-20 gap-6 justify-center h-auto">
                {exams.filter((exam) => counts[exam.exam_id] >0).map((exam) => {
                    return (
                        <div key={exam.exam_id}>
                            <div className="relative w-72 rounded-xl h-40 shadow-lg items-center justify-center">
                                <div className="absolute top-0 right-0 w-full h-1/5 bg-slate-900 rounded-t-lg text-red-500 font-medium text-center justify-center text-lg shadow-lg border-t border-r border-l border-slate-500">
                                    <h1>{exam.exam_name}</h1>
                                </div>
                                <div className="absolute bottom-0 right-0 bg-white bg-opacity-60 h-4/5 w-full rounded-b-lg text-center pt-2 border-l border-r border-slate-500">
                                    <h1>{exam.description}</h1>
                                </div>
                                <div className="absolute h-1/5 bottom-0 right-0 bg-slate-900 text-red-500 w-full text-center font-medium text-lg hover:bg-slate-600 border-l border-r border-b border-slate-500 rounded-b-lg">
                                    <Link to={`/gradebook/${exam.exam_id}`}>
                                        View Gradebook
                                    </Link>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Manage
